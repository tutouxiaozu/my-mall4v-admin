module.exports = {
    // 指定 prettier 的规则；
    'quotes': true, // 单引号还是双引号；
    'semi': true, // 行尾必须要分号结束；
    'tabWidth': 2, // tab 十几个空格
    'endOfline': 'auto', // 换行符的使用；
    'printWidth': 80,
}

