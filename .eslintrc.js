module.exports = {
    env: {
        // root: true, // 根节点,当前配置的根配置，不要从它的上级目录查找文件
        browser: true, // 运行到浏览器
        es2021: true, // 识别 es 的其他版本
        node:true,
        'vue/setup-compile-macros': true,
    },
    parser:'vue-eslint-parser',
    extends: [
        'eslint:recommended',
        'plugin:vue/vue3-recommended',
        'plugin:@typescript-eslint/recommended',
        'plugin:prettier/recommended',
        'standard'
    ],
    overrides: [
    ],
    parserOptions: {
        ecmaVersion: 'latest',
        parser:'@typescript-eslint/parser',
        sourceType: 'module'
    },
    plugins: [
        'vue',
        '@typescript-eslint'
    ],
    rules: {
        semi:'off',
        'comma-dangle':'off',
        'vue/multi-word-commponent-names':'off',
        '@typescript-eslint/no-var-requires':'off',
        // 'no-var':'error',
        // 'no-console':'error',
        // 'prettier/prettier':'error',
    }
}

// eslint-config-prettier 结局啊eslint中样式规范
