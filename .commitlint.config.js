module.exports = {
    extends:['@commitlint/config-conventional'],
    // 定义 校验规则
    rules:{
        'type-enum':[
            2,
            'always',
            [
                'feat',
                'fix',
                'bug',
                'docs',
                'style',
            ]
        ]
    }
}