import path from 'path';
import { defineConfig } from 'vite'
import vue from '@vitejs/plugin-vue';
const resolve = (dir: string) => path.join(__dirname, dir);


// https://vitejs.dev/config/
export default defineConfig({
  plugins: [vue()],
  resolve: {
    alias: {
      '@': resolve('src'),
      comps: resolve('src/components'),
      apis: resolve('src/apis'),
      views: resolve('src/views'),
      utils: resolve('src/utils'),
      routes: resolve('src/routes'),
      styles: resolve('src/styles')
    }
  },
  // 配置Dep优化行为
  optimizeDeps: {
    include: ["lodash"]
  },
  server: {
    //服务器主机名
    //端口号
    port: 8088,
    //设为 true 时若端口已被占用则会直接退出，而不是尝试下一个可用端口
    strictPort: false,
    //服务器启动时自动在浏览器中打开应用程序,当此值为字符串时，会被用作 URL 的路径名
    open: true,
    //自定义代理规则
    proxy: {
      // 选项写法
      '/api': {
        target: 'https://bjwz.bwie.com/mall4w',
        changeOrigin: true,
        rewrite: (path: any) => path.replace(/^\/api/, '')
      }
    }
  },

})
