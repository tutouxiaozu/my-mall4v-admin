// 发布订阅     典型的应用 就是 eventEmmiter

// 发布：只负责发布事件， 不关心谁 什么时候去调用（就比如报纸）；

// 调度中心 ： 存取 发布者发布的 事件；

// 订阅：   只负责订阅事件，不关心事件 什么时候被谁发布；

// 封装 EventEmmiter 类；
// EventEmmiter 

interface Listener {
    (...args: any[]): void
    fn?: Listener | null;
}

interface EventType {
    [eventName: string]: Listener[]
}

class EventEmmiter {
    // 定义一个 event
    // 存放事件
    protected events: EventType = {}

    // 发布
    // 定义 一个 emit, 有事件的时候返回 true 没有的话 返回false
    emit(eventName: string, ...ages: any[]) {
        if (!this.events[eventName]) return false
        this.events[eventName].forEach((listener: Listener) => listener(...ages))
        return true;
    }

    // on 方法 订阅
    on(eventName: string, listener: Listener) {
        // 判断该事件相对应的处理函数是否存在；
        if (this.events[eventName]) {
            this.events[eventName] = [listener];
        } else {
            this.events[eventName].push(listener)
        }
        return this;// 链式调用 返回他的实例；
    }

    // once
    once(eventName: string, listener: Listener) {
        // 定义一个 事件处理器
        const on: Listener = (...args) => listener(...args);
        on.fn = listener;
        this.on(eventName, on);
        return this;
    }

    // off(eventName: string, listener: Listener) {
    //     let $ = this.events;
    //     let arr = $[eventName];
    //     if (!arr) return
    //     for (let i = 0; i < arr.length; i++) {
    //         if (listener == arr[i]) {
    //             arr[i] = null;
    //         }
    //     }
    // }
}


export default EventEmmiter