import request from "../../utils/index";

//获取管理员列表
export const _getAdminData=(params:any)=>{
  return request.get({
      url:`/api/sys/user/page`,
      params
  })
}
//删除接口
export const _delAdminData=(userIds:any)=>{
  return request.delete({
    url:'/api/sys/user',
    data:userIds
  })
}
//修改接口数据回显
export const _editList=({userId}:any)=>{
  return request.get({
      url:`/api/sys/user/info/${userId}`
  })
}
//修改接口
export const _editData=(user:any)=>{
  return request.put({
    url:`/api/sys/user`,
    data:user
  })
}
//拿到角色列表
export const _roleList=()=>{
  return request.get({
    url:'/api/sys/role/list',
  })
}
//增加接口
export const _addData=(user:any)=>{
  return request.post({
      url:`/api/sys/user`,
      data:user
  })
}