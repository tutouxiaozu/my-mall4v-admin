
import request from '../../utils/index';

interface GetParamsT {
    t:string|number
}

// 登陆
export const _loginAdmin = async (params: any) => (
    await request.post({
        url: '/api/login?grant_type=admin',
        data:params
    })
)

// 获取路由表
export const _getMenuList = (params:GetParamsT) => (
    request.get({
        url:'/api/sys/menu/nav',
        params
    })
)

