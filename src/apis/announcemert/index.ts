
// 热搜管理
import request from '../../utils/index';

export const get_list = (params?: any) => {
    return request.get({
        url: `/api/admin/hotSearch/page`,
        params,
    })
}

export const add_list = (params: any) => {
    return request.post({
        url: `/api/admin/hotSearch`,
        data: params,
    })
}
export const del_list = (params: any) => {
    return request.delete({
        url: `/api/admin/hotSearch`,
        data: params,
    })
}
export const edit_list = (params: any) => {
    return request.put({
        url: `/api/admin/hotSearch`,
        data: params,
    })
}
export const Sear_List = (params: any) => {
    return request.get({
        url: `/api/admin/hotSearch/info/${params.id}`,
    })
}
export const Search_List = (params?: any) => {
    return request.get({
        url: `/api/admin/hotSearch/page`,
        params
    })
}
export const All_Checked = (params?: any) => {
    return request.delete({
        url: `/api/admin/hotSearch`,
        data:params
    })
}
// /admin/hotSearch / page
//shop/notice/page?t=1666958499689&current=1&size=10