import request from "../../utils/index"


export const getShopTransport = (params: any) => request.get({//运费模板
    url: "/api/shop/transport/page",
    params
})

export const shopTransport = (params: any) => request.delete({//运费模板删除
    url: "/api/shop/transport",
    data: params
})
export const shopTransportInfo = (params: any) => request.get({//运费模板编辑
    url: `/api/shop/transport/info${params.id}`
})
export const shopTransports = (params: any) => request.put({//运费模板编辑
    url: "/api/shop/transport",
    data: params
})



export const adminUserInfo = (params: any) => request.get({//会员数据编辑
    url: `/api/admin/user/info/${params.id}`,
})
export const adminUsers = (params: any) => request.put({//会员数据编辑
    url: "/api/admin/user",
    data: params
})

export const adminUser = (params?: any) => request.get({//会员数据
    url: "/api/admin/user/page",
    params
})


