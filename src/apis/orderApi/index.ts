import request from '../../utils/index';

//获取订单表格数据
export const _getOrderData = (params: any) =>{
   return request.get({
        url: `/api/order/order/page`,
        params
    })
}
//导出销售记录
export const _soleExcel= (params: any) =>{
    return request.get({
         url: `/api/order/order/soldExcel`,
         params
     })
 }
 //点击修改时弹窗里面的数据
 export const _editModal= ({orderNumber}: any) =>{
    return request.get({
         url: `/api/order/order/orderInfo/${orderNumber}`,
     })
 }