// 公告管理
import request from '../../utils/index';

export const get_list = (params?: any) => {
    return request.get({
        url: `/api/shop/notice/page`,
        params,
    })
}
export const del_list = (params: any) => {
    return request.delete({
        url: `/api/shop/notice/${params.id}`,
        data: params.date
    })
}
export const add_list = (params: any) => {
    return request.post({
        url: `/api/shop/notice`,
        data: params,
    })
}
export const cha_List = (params: any) => {
    return request.get({
        url: `/api/shop/notice/info/${params}`,
    })
}
export const edit_list = (params: any) => {
    return request.put({
        url: `/api/shop/notice`,
        data: params,
    })
}
// edit_list
