
import request from '../../utils/index';

export const _getProductData = (params: any) => (
    request.get({
        url: `/api/prod/prod/page?size=${params.size}&current=${params.current}`,
    })
) 