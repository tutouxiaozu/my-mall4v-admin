import request from "../../utils/index";
//菜单管理
export const _menu_management = () => (
    request.get({
        url: `/api/sys/menu/table`,
    })
) 

//定时任务------获取数据
export const _menu_timed = (params?:any) =>(
    request.get({
        url:`/api/sys/schedule/page`,
        params
    })
)
//定时任务------添加数据
export const _add_menu_timed = (params?:any)=>(
    request.post({
        url:`/api/sys/schedule`,
        data:params
    })
)   
//定时任务------回显数据
export const _echo_data = (params?:any,t?:any)=>{
    return request.get({
        url:`/api/sys/schedule/info/${params}`,
    })
}
//定时任务------修改数据
export const _edit_menu_timed = (params?:any)=>{
   return request.put({
        url:`/api/sys/schedule`,
        data:params
    })
}
//定时任务------暂停数据
export const suspend_timed = (params?:any)=>{
   return request.post({
        url:`/api/sys/schedule/pause`,
        data:params
    })
}
//定时任务------恢复数据
export const resume_timed = (params?:any)=>{
    return request.post({
         url:`/api/sys/schedule/resume`,
         data:params
     })
 }
 //定时任务------立即执行
export const now_timed = (params?:any)=>{
    return request.post({
         url:`/api/sys/schedule/run`,
         data:params
     })
 }
  //定时任务------立即删除
export const del_timed = (params?:any)=>{
    return request.delete({
         url:`/api/sys/schedule`,
         data:params
     })
 }
 


//参数管理
export const menu_paramerter = (params?:any)=>(
    request.get({
        url:`/api/sys/config/page`,
        params
    })
)
//参数管理---添加数据
export const add_paramerter = (params?:any)=>(
    request.post({
        url:`/api/sys/config`,
        data:params
    })
)
//参数管理---回显数据
export const echo_paramerter = (params?:any)=>(
    request.get({
        url:`/api/sys/config/info/${params}`,
    })
)
//参数管理---修改数据
export const edit_paramerter = (params?:any)=>(
    request.put({
        url:`/api/sys/config`,
        data:params
    })
)
//参数管理---删除数据
export const del_paramerter = (params?:any)=>(
    request.delete({
        url:`/api/sys/config`,
        data:params
    })
)


//系统日志
export const menu_Parame = (params?:any)=>(
    request.get({
        url:`/api/sys/log/page`,
        params
    })
)
