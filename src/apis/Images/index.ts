// 轮播图管理
import request from '../../utils/index';

export const get_list = (params?: any) => {
    return request.get({
        url: `/api/admin/indexImg/page`,
        params,
    })
}
export const del_list = (params: any) => {
    return request.delete({
        url: `/api/admin/indexImg`,
        data: params,
    })
}
export const search_list = (params: any) => {
    return request.get({
        url: `/api/admin/indexImg/info/${params}`,
    })
}
// /admin/indexImg/info/137
