import request from "../../utils";

//获取角色列表数据
export const _getRoleData=(params?:any)=>{
    return request.get({
        url:'/api/sys/role/page',
        params
    })
}
//删除接口
export const _delRoleData=(params:any)=>{
    return request.delete({
        url:`/api/sys/role`,
        data:params
    })
}
//获取增加弹框里面的折叠数据
export const _addList=()=>{
    return request.get({
        url:'/api/sys/menu/table'
    })
}
//修改接口数据回显
export const _editList=({roleId}:any)=>{
    return request.get({
        url:`/api/sys/role/info/${roleId}`
    })
}
//修改接口
export const _editData=(role:any)=>{
    return request.put({
        url:`/api/sys/role`,
        data:role
    })
}
//增加接口
export const _addData=(role:any)=>{
    return request.post({
        url:`/api/sys/role`,
        data:role
    })
}