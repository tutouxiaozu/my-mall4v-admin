import request from '../../utils/index';

// 获取info
export const _getHomeInfo = async (params: any) => {
    return await request.get({
        url: `/api/sys/user/info?=${params}`,
    })
}
// 获取动态路由
export const _getHomeNav = async (params: any) => (
    await request.get({
        url: `/api/sys/menu/nav?=${params}`,
    })
)