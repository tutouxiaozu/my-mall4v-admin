import request from "../../utils";

//获取地址列表数据
export const _getAreaData=(params:any)=>{
    return request.get({
        url:'/api/admin/area/list',
        params
    })
}