
// 分组管理
import * as T from './index.d';
import http from '../../utils/index';
// 表格数据
export const _getGroupingPage = (params?: T.GetProdTagPage) => (
    http.get({
        url: '/api/prod/prodTag/page',
        params
    })
)
// 删除 
export const _deleteGroupingPag = (id?: number) => (
    http.delete({
        url: `/api/prod/prodTag/${id}`,
    })
)

// 新增
export const _addGroupingPage = (params: T.PostAddYagPag) => (
    http.post({
        url:'/api/prod/prodTag',
        data:params
    })
)

// 编辑
export const _editGroupingPage = (params:T.PutEditTagPage) => (
    http.put({
        url:'/api/prod/prodTag',
        data:params
    })
)