// 请求数据参数
export interface GetProdTagPage {
    size: number,
    current: number,
    title: string | null,
    status: string | number | null,
    t: number
}

// 请求新增的时候的数据
export interface PostAddYagPag {
    id?: number,
    isDefault?: number | null | string,
    prodCount?: number | null | string,
    seq?: string | null | number | undefined,
    shopId?: number | null | string,
    status: number,
    style: number,
    title: string,
    t?: number
}

// 编辑参数
export interface PutEditTagPage {
    createTime: string,
    deleteTime: any,
    id: number,
    isDefault?: number | null | string,
    prodCount?: number | null | string,
    seq?: string | null | number | undefined,
    shopId?: number | null | string,
    status: number,
    style: number,
    title: string,
    t?: number,
    updateTime: string
}