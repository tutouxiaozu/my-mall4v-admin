// 主文件
import useLoginPinia from './module/login';
import CrumbsPinia from './module/crumbs';
import IndexPinia from './module/index';
import ProdPinia from './module/prod';

export default function useStore() {
    return {
        login: useLoginPinia(), // 登录
        index: IndexPinia(), // 首页
        crumbs: CrumbsPinia(), // 面包屑
        prod: ProdPinia(), // 产品管理
    }
}
