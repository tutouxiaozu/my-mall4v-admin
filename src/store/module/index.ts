import { defineStore } from 'pinia';
import router from '../../router/index';
import LocalStor from '../../utils/utils';
import * as http from '../../apis/indexApi/index';
import { getRoutes } from '../../utils/permission';

const IndexPinia = defineStore('indexPinia', {
    state() {
        return {
            userInfo: '麻辣猪蹄',
            menuList: <any>[],
            authorities: <any>[]
        }
    },
    actions: {
        async _getMenuData() {
            let res: any = await http._getMenuList({
                t: new Date().getTime()
            })
            if (res.status == 200) {
                this.menuList = res.data.menuList;
                LocalStor.setCache('menuList', this.menuList); // 将路由存入本地;
                // this.menuList
                // this.changMenu(res.data.menuList);
            }
        },
        changMenu(payload: any) {
            const routes = getRoutes(payload);
            
            routes.forEach(item => {
                router.addRoute('home', item)
            })
        },
        getMenu() {
            // 防止刷新的时候数据丢失；
            // 舒心的时候 重新获取 menu ，从本地获取
            const menuList = LocalStor.getCache('menuList');
            
            // const authorities = LocalStor.getCache('authorities');

            if (menuList) {
                this.menuList = [...menuList];
                // this.authorities = [...authorities];
                // 动态添路由 
                this.changMenu([...menuList])
            }
        },
        RefreshToken() {
            const refToken = LocalStor.getCache('refresh_token');
            const expires_in = LocalStor.getCache('expires_in');
            const sessToken = window.sessionStorage.getItem('token');
            // 重新进入页面的时候 判断 session中是否有token
            if (!sessToken) {
                console.log('重新请求数据 刷新token');
                LocalStor.setCache('token', 'token');
            }
            setTimeout(() => {
                // 
            }, expires_in);
        }
    }
})

export default IndexPinia