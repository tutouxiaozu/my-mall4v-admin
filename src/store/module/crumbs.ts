import { defineStore } from 'pinia';
import * as T from '../types/crumbs'

const crumbsPinia = defineStore('crumbsPinia', {
    state() {
        return {
            crumbsList: <any>[
                {
                    menuId:0,
                    name:'首页'
                }
            ]
        }
    },
    actions: {
        addRoutes(payload: T.CrumbsListType) {
            const flag = this.crumbsList.some((item: any) => item.menuId === payload.menuId)

            if (!flag) { // 如果数据中不存在 该数据
                this.crumbsList.push(payload);
            } else return

            console.log(this.crumbsList, 'this.crumbsList');

        }
    }
})

export default crumbsPinia;