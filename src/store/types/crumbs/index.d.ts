

export interface CrumbsListType {
    icon:string,
    list:any,
    menuId:number,
    name:string,
    orderNum:number,
    parentId:number,
    parentName:string|null,
    perms:string,
    type:number,
    url:string
}