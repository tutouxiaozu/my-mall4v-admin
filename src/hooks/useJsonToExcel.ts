import {utils,writeFile,BookType} from "xlsx"

const useJsonToExcel=(options:{
    arr:any[];
    header:any,
    fileName:string,
    bookType:BookType
})=>{
  //创建工作簿
  //book_new是创建工作簿的方法
  const exSheet=utils.book_new();
  //创建工作表
  if(options.header){
      //转换data
      options.arr=options.arr.map((item:any)=>{
          //定义一个对象
          const obj:any={};
          //让表头和数据关联起来
          for(const key in item){
              obj[options.header[key]]=item[key]
          }
          //返回处理好的数据
          return obj;
      })
  }
  //转换数据 把数据转到sheet页里
  const ts=utils.json_to_sheet(options.arr);
  //把sheet也放到工作簿
  utils.book_append_sheet(exSheet,ts);
  //生成数据
  writeFile(exSheet,options.fileName,{
      bookType:options.bookType
  });
}
export default useJsonToExcel
// // 导出 Excel 表格
// import { utils, writeFile, BookType } from 'xlsx';
// // utils 人家封装的工具
// // writeFile 写入到文件
// // BookType 他的类型

// const userJsonToExcel = (options: {
//     data: any, // 表的数据
//     header: any, // 标头信息
//     fileName: string, // 文件名字
//     bookType: BookType // 后缀
// }) => {
//     // 1. 创建一个 工作簿 book_new创建工作薄的方法;
//     const exSheet = utils.book_new();
//     // 2. 创建工作表
//     if (options.header) {
//         // 转换 data
//         options.data = options.data.map((item: any) => {
//             // 定义一个对象接受
//             const obj: any = {};
//             for (let key in item) {
//                 obj[options.header[key]] = item[key]
//             }
//             // 返回 表头 和 数据 结合到一起的对象;
//             return obj;
//         })
//     }

//     // 2.2 创建sheet页;
//     const sheetPag = utils.json_to_sheet(options.data);
//     // 2.3 将sheet页放入工作薄中;
//     utils.book_append_sheet(exSheet, sheetPag);

//     // 3 生成数据(生成文件)
    
//     // exSheet 数据;fileName文件名;
//     writeFile(exSheet,options.fileName,{
//         bookType:options.bookType
//     });
// }

// export default userJsonToExcel;
