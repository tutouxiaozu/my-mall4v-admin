const handleCommentList = (dataList: any, id: any, list: any) => {
    for (let item of dataList) {
        if (item.parentId === id) {
            list.push(item);
        }
    }
    for (let i of list) {
        i.children = [];
        handleCommentList(dataList, i.categoryId, i.children);
    }
    return list
}

export default handleCommentList;
