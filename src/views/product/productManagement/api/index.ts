import * as T from '../types/index.d';
import request from '../../../../utils/index';

// 获取数据；搜索;
export const _searchPardPage = (params?: T.SearchParamsType) => (
    request.get({
        url: '/api/prod/prod/page',
        params
    })
)

// 新增 弹框 产品分类
export const _addClassifyProdPage = (params: any) => (
    request.get({
        url: '/api/prod/category/listCategory',
        params
    })
)

// 新增 弹框 产品分组
export const _addGroupingProdPage = (params: any) => (
    request.get({
        url: '/api/prod/prodTag/listTagList',
        params
    })
)

// 规格名
export const _addSpecificationsProdPage = (params: any) => (
    request.get({
        url: '/api/prod/spec/list',
        params
    })
)

// 规格名 筛选 规格值的数据
export const _SpecificationsNumPage = (params: any) => (
    request.get({
        url: `/api/prod/spec/listSpecValue/${params}`,
    })
)

// 表格删除数据
export const _deleteprodPage = (params: any) => (
    request.delete({
        url: ''
    })
)

// 运费 设置
export const _freightData = (params: any) => (
    request.get({
        url: '/api/shop/transport/list',
        params
    })
)

// 规格名 设置
export const _specificationData = (params: any) => (
    request.get({
        url: '/api/prod/spec/list',
        params
    })
)

// 运费设置 表格数据
export const _freightTabData = (params: any) => (
    request.get({
        url:`/api/shop/transport/info/${params}`
    })
)