
// 搜索参数
export interface SearchParamsType {
    t?: number | string,
    current: number | string,
    size: number | string,
    prodName?: string,
    status?: number | string
}