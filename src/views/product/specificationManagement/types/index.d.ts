

export interface GetSpecificationListType {
    size: number,
    current: number,
    propName: string,
    t: number
}