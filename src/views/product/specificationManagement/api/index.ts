import request from '../../../../utils/index';
import * as T from '../types/index.d';
// /prod/spec/page
// 获取 规格管理页面的数据
export const _getSpecificationList = (params:T.GetSpecificationListType) => (
    request.get({
        url:'/api/prod/spec/page',
        params
    })
)

// 删除 项
export const _deleteSpecificationItem = (params:number) => (
    request.delete({
        url:`/api/prod/spec/${params}`
    })
)

// 编辑 新增事件
export const _putSpecification = (params:any) => {
    request.put({
        url:'/api/prod/spec',
        data:params
    })
}