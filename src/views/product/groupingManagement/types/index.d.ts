// 搜索传输的数据
export interface SearchFromType {
    title: string,
    status: string,
}

// 请求的渲染数据
export interface GroupingType {
    current: number,
    pages: number,
    searchCount: boolean,
    total: number,
    size: number,
    records:GroupingTableType[]
}
// 表格的数据
export interface GroupingTableType {
    createTime:string,
    deleteTime:string | null,
    id:number,
    isDefault:number,
    prodCount:number,
    seq:number,
    shopId:number,
    status:number,
    style:number,
    title:string,
    updateTime:string,
}