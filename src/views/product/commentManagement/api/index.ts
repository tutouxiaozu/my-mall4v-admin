import request from '../../../../utils/index';

// 请求表格数据
export const _getCommentList = (params: any) => (
    request.get({
        url: '/api/prod/category/table',
        params
    })
)

// 上级分类 数据
export const _getCommentClassifyList = (params: any) => (
    request.get({
        url: '/api/prod/category/listCategory',
        params
    })
)

// 表格 点击删除的时候;
export const _deleteComment = (params: any) => {
    request.delete({
        url: '/api/prod/category/delete',
        data: params
    })
}