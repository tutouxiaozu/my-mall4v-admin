import './style.css';
import App from './App.vue';
import { createApp } from 'vue';
import { createPinia } from 'pinia';
import router from './router/index';
import 'ant-design-vue/dist/antd.css';
import ElementPlus from 'element-plus';
import { DatePicker } from 'ant-design-vue';
import '../node_modules/element-plus/dist/index.css'
import * as ElementPlusIconsVue from '@element-plus/icons-vue'
import useStore from './store/index';

const app = createApp(App);

const pinia = createPinia()

app.use(pinia)

const { index } = useStore();

for (const [key, component] of Object.entries(ElementPlusIconsVue)) {
    app.component(key, component)
}

index.getMenu();

app.use(router).use(ElementPlus).use(DatePicker).mount('#app')
