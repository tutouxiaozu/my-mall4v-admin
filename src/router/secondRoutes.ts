const SecondRoutes = [
    {
        path: '/index/prod',
        name: '产品管理',
        children: [
            {
                path: '/index/prod/prodTag',
                name: '分组管理',
                component: () => import('@/views/product/groupingManagement/index.vue'),
            },
            {
                path: '/index/prod/prodList',
                name: '产品管理',
                component: () => import('@/views/product/productManagement/index.vue'),
            },
            {
                path: '/index/prod/category',
                name: '评论管理',
                component: () => import('@/views/product/classifyManagement/index.vue'),
            },
            {
                path: '/index/prod/prodComm',
                name: '分类管理',
                component: () => import('@/views/product/commentManagement/index.vue'),
            },
            {
                path: '/index/prod/spec',
                name: '规格管理',
                component: () => import('@/views/product/specificationManagement/index.vue'),
            },
        ],
    },
    {
        path: '/index/shop',
        name: '门店管理',
        children: [
            {
                path: '/index/shop/selfPick_upSite',
                name: '自提点管理',
                component: () => import('@/views/shop/selfPick_upSiteManagement/index.vue'),
            },
            {
                path: '/index/shop/freightTemplate',
                name: '运费模板',
                component: () => import('@/views/shop/freightTemplate/index.vue'),
            },
            {
                path: '/index/shop/swiper',
                name: '轮播管理',
                component: () => import('@/views/shop/swiperManagement/index.vue'),
            },
            {
                path: '/index/shop/topSearch',
                name: '热搜管理',
                component: () => import('@/views/shop/topSearchManagement/index.vue'),
            },
            {
                path: '/index/shop/announcement',
                name: '公告管理',
                component: () => import('@/views/shop/announcementManagement/index.vue'),
            },
        ],
    },
    {
        path: '/index/members',
        name: '会员管理',
        children: [
            {
                path: '/index/members/memberManagement',
                name: '会员管理',
                component: () => import('@/views/member/index.vue'),
            }
        ],
    },
    {
        path: '/index/order',
        name: '订单管理',
        children: [
            {
                path: '/index/order/ordeManagement',
                name: '订单管理',
                component: () => import('@/views/order/index.vue'),
            }
        ],
    },
    {
        path: '/index/system',
        name: '系统管理',
        children: [
            {
                path: '/index/system/address',
                name: '地址管理',
                component: () => import('@/views/system/addressManagement/index.vue'),
            },
            {
                path: '/index/system/Administrator',
                name: '管理员列表',
                component: () => import('@/views/system/Administrator/index.vue'),
            },
            {
                path: '/index/system/role',
                name: '角色管理',
                component: () => import('@/views/system/roleManagement/index.vue'),
            },
            {
                path: '/index/system/menu',
                name: '菜单管理',
                component: () => import('@/views/system/menuManagement/index.vue'),
            },
            {
                path: '/index/system/timedTask',
                name: '定时任务',
                component: () => import('@/views/system/timedTask/index.vue'),
            },
            {
                path: '/index/system/parameter',
                name: '参数管理',
                component: () => import('@/views/system/parameterManagement/index.vue'),
            },
            {
                path: '/index/system/systemTask',
                name: '系统日志',
                component: () => import('@/views/system/systemTask/index.vue'),
            },
        ],
    },
]
export default SecondRoutes