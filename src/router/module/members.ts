const membersRoutesList = [
    {
        path: '/user/user',
        name: '会员管理',
        component: () => import('@/views/member/index.vue'),
    }
]

export default membersRoutesList;