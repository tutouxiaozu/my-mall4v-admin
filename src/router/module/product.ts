const routesList = [
    {
        path: '/prod/prodTag',
        name: '分组管理',
        component: () => import('../../views/product/groupingManagement/index.vue'),
    },
    {
        path: '/prod/prodList',
        name: '产品管理', 
        component: () => import('@/views/product/productManagement/index.vue'),
    },
    {
        path: '/prod/category',
        name: '评论管理',
        component: () => import('@/views/product/classifyManagement/index.vue'),
    },
    {
        path: '/prod/prodComm',
        name: '分类管理',
        component: () => import('@/views/product/commentManagement/index.vue'),
    },
    {
        path: '/prod/spec',
        name: '规格管理',
        component: () => import('@/views/product/specificationManagement/index.vue'),
    },
]

export default routesList