const shopRoutesList = [
    {
        path: '/shop/pickAddr',
        name: '自提点管理',
        component: () => import('@/views/shop/selfPick_upSiteManagement/index.vue'),
    },
    {
        path: '/shop/transport',
        name: '运费模板',
        component: () => import('@/views/shop/freightTemplate/index.vue'),
    },
    {
        path: '/admin/indexImg',
        name: '轮播管理',
        component: () => import('@/views/shop/swiperManagement/index.vue'),
    },
    {
        path: '/shop/hotSearch',
        name: '热搜管理',
        component: () => import('@/views/shop/topSearchManagement/index.vue'),
    },
    {
        path: '/shop/notice',
        name: '公告管理',
        component: () => import('@/views/shop/announcementManagement/index.vue'),
    },
]

export default shopRoutesList;