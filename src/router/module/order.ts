const orderRoutesList = [
    {
        path: '/order/order',
        name: '订单管理',
        component: () => import('@/views/order/index.vue'),
    }
]

export default orderRoutesList;