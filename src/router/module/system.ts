const systemRoutesList = [
    {
        path: '/sys/area',
        name: '地址管理',
        component: () => import('@/views/system/addressManagement/index.vue'),
    },
    {
        path: '/sys/user',
        name: '管理员列表',
        component: () => import('@/views/system/Administrator/index.vue'),
    },
    {
        path: '/sys/role',
        name: '角色管理',
        component: () => import('@/views/system/roleManagement/index.vue'),
    },
    {
        path: '/sys/menu',
        name: '菜单管理',
        component: () => import('@/views/system/menuManagement/index.vue'),
    },
    {
        path: '/sys/schedule',
        name: '定时任务',
        component: () => import('@/views/system/timedTask/index.vue'),
    },
    {
        path: '/sys/config',
        name: '参数管理',
        component: () => import('@/views/system/parameterManagement/index.vue'),
    },
    {
        path: '/sys/log',
        name: '系统日志',
        component: () => import('@/views/system/systemTask/index.vue'),
    },
]

export default systemRoutesList;