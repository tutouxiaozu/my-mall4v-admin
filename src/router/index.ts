import { createRouter, createWebHistory } from 'vue-router';
import routesList from './module/product';
// 开启历史模式
// vue2中使用的mode：history 实现;

const routes: any = [
    {
        path: '/',
        redirect: '/home'
    },
    {
        path: '/home',
        name: 'home',
        component: () => import('@/views/index/index.vue'),
        children: [
            {
                path: '/home/homepage',
                name: 'homePage',
                component: () => import('@/views/homePage/index.vue')
            },
            {
                path: '/prodInfo',
                name: 'prodInfo',
                component: () => import('@/views/product/productManagement/components/productUtils/index.vue')
            },
            {
                path: '/home',
                redirect: '/home/homepage'
            },
            
        ]
    },
    {
        path: '/login',
        component: () => import('@/views/login/index.vue')
    },
    {
        path: '/:pathMatch(.*)',
        conpoment: () => import('@/views/404/inde.vue')
    },
]

const router = createRouter({
    history: createWebHistory(),
    routes
});

export default router;