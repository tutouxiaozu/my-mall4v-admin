import axios, { AxiosRequestConfig, AxiosInstance } from 'axios';
// AxiosRequestConfig 请求的类型；
// AxiosInstance 实例的类型；

// 引入 提示框，loading 加载的效果
import { ElMessage, ElLoading } from 'element-plus';
import { LoadingInstance } from 'element-plus/lib/components/loading/src/loading';
import localStor from './utils';

// 定义 是否需要显示 loading 加载效果；
const DEFAULT_LOADING = true;

interface HttpRequestConfig extends AxiosRequestConfig {
    isShowLoading?: boolean
}

class HttpRequest {
    instance: AxiosInstance | undefined
    isShowLoading: boolean | undefined
    isLoading?: LoadingInstance
    constructor(config: HttpRequestConfig) {
        this.instance = axios.create(config); // 创建 axios 实例

        this.isShowLoading = config.isShowLoading || DEFAULT_LOADING

        // 添加拦截器  请求拦截  interceptors:拦截器
        this.instance.interceptors.request.use(configs => {
            // 是否需要加载 loading 效果；
            if (this.isShowLoading) {
                // 通过 isShowLoading 如果需要加载效果，就在发送请求之前 显示 loading 效果
                this.isLoading = ElLoading.service({
                    lock: true, // 是否展示
                    text: '正在请求中', // 正在加载的时候显示的文字
                    background: 'rgba(0,0,0,.6)' // 遮罩的 备件颜色
                })
            }

            this.isShowLoading = false;
            // 获取 token；把token 放入请求头中；
            const token = localStor.getCache('token');

            return {
                ...configs,
                headers: {
                    ...configs.headers,
                    Authorization: token
                }
            }

        }, error => {
            // 请求失败的 时候
            return Promise.reject(error);
        })

        // 响应拦截, 在服务器响应之后  关闭loading效果
        this.instance.interceptors.response.use(response => {
            // 不管是相应成功，还是失败都要关闭 loading效果
            this.isLoading?.close();
            return response;
        }, error => {
            this.isLoading?.close()
            // 需要 针对 后端相应的 http 状态码 做处理；
            const { code } = error.response;
            switch (code) {
                case 400:
                    ElMessage.error('请求失败，客户端可能语法有误，服务端不能理解请求')
                    break;
                case 401:
                    ElMessage.error('没有访问权限')
                    break;
                case 403:
                    ElMessage.error('页面重定向')
                    break;
                case 404:
                    ElMessage.error('客户端地址有误')
                    break;
                case 500:
                    ElMessage.error('服务端发生了不可预知的错误，请联系管理员')
                    break;

            }
            return Promise.reject(error);
        })
    }

    request<T>(config: HttpRequestConfig): Promise<T> {
        return new Promise((resolve, reject) => {
            this.instance?.request<any, T>(config).then(res => {
                // 更改 isShowLoading 的值；
                this.isShowLoading = DEFAULT_LOADING;
                resolve(res);
            }).catch(error => {
                this.isShowLoading = DEFAULT_LOADING;
                reject(error);
                return error;
            })
        })
    }

    get<T>(config:any):Promise<T> {
        return this.request<T>({
            ...config,
            method: 'GET',
        })
    }
    post<T>(config:any):Promise<T> {
        return this.request<T>({
            ...config,
            method: 'post',

        })
    }
    put<T>(config:any):Promise<T> {
        return this.request<T>({
            ...config,
            method: 'put',

        })
    }
    delete<T>(config:any):Promise<T> {
        return this.request<T>({
            ...config,
            method: 'delete',

        })
    }

}

export default HttpRequest;