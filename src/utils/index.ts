import HttpRequest from './request';

const request = new HttpRequest({
    baseURL:import.meta.env.BASE_URL,
    timeout:200000,
})
export default request;
