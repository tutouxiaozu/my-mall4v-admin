
const timestampToTime = (times: any) => {
    let time = times[1]
    let mdy = times[0]
    mdy = mdy.split('/')
    let month = parseInt(mdy[0]);
    let day = parseInt(mdy[1]);
    let year = parseInt(mdy[2])
    return year + '-' + month + '-' + day + ' ' + time
}
let time = new Date()
export let nowTime = timestampToTime(time.toLocaleString('en-US', { hour12: false }).split(" "))
