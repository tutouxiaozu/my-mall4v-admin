import axios from 'axios';
// 使用由库提供的配置的默认值来创建实例
// 此时超时配置的默认值是 `0`
var instance = axios.create({
    baseURL: 'https://bjwz.bwie.com/mall4w'
});
// 添加请求拦截器
instance.interceptors.request.use(config => {
    // 在发送请求之前做些什么
    // 'Authorization', 'bearer' + data.access_token
    // window.localStorage.getItem('token');
    const token = window.localStorage.getItem('token');
    return {
        ...config,
        headers: {
            ...config.headers,
            Authorization: 'Bearer ' + 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjhmODE4YjdlLWNjZjgtNDFiOS1hMGFkLWQ5ZTliZmI0MzgwNCIsIm5hbWUiOiJ6aG91YmluIiwiZW1haWwiOiIxMjM0NTY3ODkxMEBxcS5jb20iLCJyb2xlIjoiYWRtaW4iLCJpYXQiOjE2NjUxMTU4NDUsImV4cCI6MTY2NTEzMDI0NX0.zNMk0Ej-WP0qkaDcck4lUOvX3poOexEvv4KIXDSfDcQ'
        }
    };
}, function (error) {
    // 对请求错误做些什么
    return Promise.reject(error);
});

// 添加响应拦截器
instance.interceptors.response.use(function (response) {
    // 对响应数据做点什么
    return response;
}, function (error) {
    // 对响应错误做点什么
    return Promise.reject(error);
});

export default instance