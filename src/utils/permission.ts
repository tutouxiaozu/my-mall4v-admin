
//
import type { RouteRecordRaw } from 'vue-router'
import router from '../router/index';

export const getRoutes = (menuList: any) => {

    // 最终返回的路由(筛选过后的路由)；
    const routes: RouteRecordRaw[] = [];

    // 拿到所有的路由；
    let allRoutes: any = [];

    // 获取 router 件文中的路由；
    const routeFiles: any = import.meta.globEager('@/router/module/*.ts');

    // 处理 router 文件夹中的数据;
    allRoutes = Object.keys(routeFiles).reduce((prev: any, next: any) => {
        prev.push(...routeFiles[next].default);
        return prev;
    }, []);


    const changeRoutes = (menu: any) => {
        for (const item of menu) {
            // 判断 type 类型
            if (item.type === 1) {
                const route = allRoutes.find((val: any) => val.path === '/' + item.url)
                if (route) routes.push(route)
            } else {
                if (item.list !== null) {
                    changeRoutes(item.list)
                }
            }
        }
    }

    changeRoutes(menuList);


    return routes;
}
